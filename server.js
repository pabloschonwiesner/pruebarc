const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

require('./config/config');
const controller = require('./controller/controller');

app.use(cors());
app.use(bodyParser.urlencoded({ 'extended': true}));
app.use(bodyParser.json());


app.get('/', (req, res) => res.json( { 'ok': true }));
app.get('/recursos', controller.recursos);

app.listen(process.env.PORT, () => console.log(`Escuchando en el puerto ${process.env.PORT}`));