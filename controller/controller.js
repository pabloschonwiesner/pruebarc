const configDB = require('./../config/configDB');
const sql = require('mssql');

console.log(configDB)
const controller = {
  recursos: function(req, res) {
    sql.close();
    sql.connect(configDB)
      .then(conn => {
        return new sql.Request()
          .execute('SPPruebaRecursos')
      })
      .then(result => {
        var obj = JSON.parse(JSON.stringify(result.recordsets[0]));
        res.status(200).json({
          'ok': true,
          'total_registros': obj.length,
          'data': obj,
          'err': ''
        })
      })
      .then(() => sql.close())
      .catch((err) => {
        console.log(`Error en la llamada recursos a la BD: ${err}`);
        res.status(500).json( { 
          'ok': false, 
          'total_registros': 0,
          'data': [],
          'err': err
        })
      })
  }
}

module.exports = controller;
